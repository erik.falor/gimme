(print "test.scm")
(load "gimme.scm")

(define one
  (string-split "a bunch of words, then a thousand words, followed by ten nouns"))

(define two
  (string-split
    "two hundred words, five thousand words, seventy nine words"))

(define three
  (string-split
    "two hundred fifty seven words, five thousand, five hundred and fifty five words"))

(define four
  (string-split
    "
    twenty four thousand three hundred colors,
    twenty four thousand three hundred ten colors,
    four thousand three hundred fifty six words,
    "))

(define five
  (string-split
    "fifteen hundred nouns
    fifteen thousand one hundred and forty adjectives
    fifty seven hundred words"))

(define six
  (string-split
    "
    one hundred eighty seven adjectives
    four thousand fifty six names,
    four thousand fifty colors,
    twenty four thousand colors,
    twenty seven thousand verbs
    "))


