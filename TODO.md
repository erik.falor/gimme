Bugs
====

`make` fails b/c my cute little test fails when the dictionaries aren't yet installed
Maybe I should give `gimme` a cmdline arg that overrides the path to `dict/`?

Word lists
==========

Alan Beale's 12dicts word lists:

*   http://wyrdplay.org/12dicts.html v6.0.2
*   http://wordlist.aspell.net/12dicts/

This is a Part-of-Speech database:

*   http://wordlist.aspell.net/other/

Here are some other files I might use:

*   http://www.manythings.org/vocabulary/lists/l/
*   https://www.keithv.com/software/wlist/
