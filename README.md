## Bug: `$ gimme a number between 1 and 2` will *always* return 1

This is because the vector `#(1 2)` is treated with the Fisher-Yates shuffle, resulting in `#(2 1)`.  One element of the shuffled vector is returned *starting from the last element*.
