#!/usr/bin/csi -s

(import (chicken bitwise))
(import (chicken blob))
(import (chicken file))
(import (chicken io))
(import (chicken pathname))
(import (chicken process-context))
(import (chicken random))
(import (chicken string))
(import (chicken time posix))
(import srfi-1)
(import srfi-13)
(import srfi-14)
(import srfi-4)


(define *VERSION* "1.12")

(define *SEARCH-PATHS* '("/usr/share" "/usr/local/share"))

;; Locate a file by searching paths contained in *SEARCH-PATHS*
(define (which filename)
  (define (file-read-access? f)
    (and (file-exists? f) (file-readable? f)))

  (let loop ((paths *SEARCH-PATHS*))
    (cond
      ((null? paths)
       (print (string-join (cons (conc "Dictionary " filename " not found in search paths") *SEARCH-PATHS*)))
       #f)
      (else
        (let ((path (car paths)))
          (cond
            ; look for file in component
            ((file-read-access? (conc path "/" filename))
             (conc path "/" filename))

            ; look for file in dirname of component
            ((file-read-access? (conc (pathname-directory path) "/" filename))
             (conc (pathname-directory path) "/" filename))

            (else
              (loop (cdr paths)))))))))


;; manually randomize the PRNG from /dev/urandom otherwise the current $SECONDS
;; are used, resulting the stable outputs when the program is run quickly
;; back-to-back
(define (seed-prng-from-urandom!)
  (set-pseudo-random-seed!
    (with-input-from-file "/dev/urandom" (lambda () (read-string 8)))))

;; Fisher-Yates shuffle on a vector
(define (vec-fy-shuffle! vec)
  (let loop ((end (sub1 (vector-length vec))))
    (if (zero? end)
        vec
        (let* ((i (pseudo-random-integer end))
               (tmp (vector-ref vec i)))
          (vector-set! vec i (vector-ref vec end))
          (vector-set! vec end tmp)
          (loop (sub1 end))))))


;; pick up to N items from a vector, or the entire vector if N exceeds the
;; number of elements
(define (pick vec n)
  (if (>= n (vector-length vec))
      (vector->list vec)
      (unfold
        zero?
        (lambda (i) (vector-ref vec i))
        sub1
        n)))


;; Return a number in the range [i, j)
(define (a-number-between i j)
  (let ((spread (add1 (abs (- j i)))))
    (+ (min i j) (pseudo-random-integer spread))))



;;;; In the functions below, a "spec", or a "gimme specification" is a pair
;;;; (kind . quantity)

;; Select words at random from a dictionary
(define (random-words spec)
  (let ((kind (car spec))
        (n (cdr spec)))
    ; choose a file
    (and-let* ((filename (which (conc "dict/" (symbol->string kind))))
           ; slurp it in
           (lst (with-input-from-file
                  filename
                  (lambda ()
                    (unfold-right eof-object? values
                                  (lambda (x) (read-line)) (read-line)))))
           (vect (vec-fy-shuffle! (list->vector lst))))

      ; pick n random elements
      (pick vect n))))


;; Pick a range of numbers
(define (number spec)
  (pick
    (or
      (and-let* (((not (null? (caddr spec))))
                 (fst (string->number (caaddr spec)))
                 (snd (string->number (car (cdaddr spec))))
                 ((and fst snd))
                 (lo (min fst snd))
                 (hi (add1 (max fst snd))))
                (vec-fy-shuffle! (list->vector (iota (- hi lo) lo))))
      (vec-fy-shuffle! (list->vector (iota 201 -100))))
    (cadr spec)))


;; Flip a fair coin `spec` times
(define (coin-flip spec)
  (define (flip-64-coins)
    (define bv (random-bytes (make-blob 8))) ; an 8-byte blob of random data
    (define u64v (blob->u64vector bv))
    (do ((u64v (u64vector-ref u64v 0))
         (i 0 (+ i 1))
         (flips '() (cons (bit->boolean u64v i) flips)))
      ((= i 64) 
       (map (lambda (f) (if f 'Heads 'Tails)) flips))))

  (let-values (((q r) (quotient&remainder (cdr spec) 64)))
    (let loop ((q q) (flips '()))
      (cond
        ((zero? q) (append flips (take (flip-64-coins) r)))
        (else      (loop (sub1 q) (append flips (flip-64-coins))))))))


;; generate random hex colors
(define (hex-color spec)
  (define (two-digits-padded s)
    (string-pad s 2 #\0))
  (define (hexify v i)
    (two-digits-padded (number->string (u8vector-ref v i) 16)))
  (define (make-color)
    (let* ((bv (random-bytes (make-blob 3)))
           (v (blob->u8vector bv))
           (r (hexify v 0))
           (g (hexify v 1))
           (b (hexify v 2)))
      (conc #\# r g b)))
  (let loop ((n (cdr spec)))
    (cond
      ((zero? n) '())
      (else (cons (make-color) (loop (sub1 n)))))))


;; Report the current time
(define (local-time spec)
  (unfold zero? (lambda x (seconds->string)) sub1 (cdr spec)))


;; Coalesce a list of quantities
;; 
;; '(four thousand three hundred fifty six) -> (4 1000 3 100 50 6) -> (4000 300 56) -> 4356
;; '(four thousand fifty six) -> (4 1000 50 6) -> (4000 56) -> 4056
;; '(four thousand fifty) -> (4 1000 50) -> (4000 50) -> 4050
;;
;; The algorithm looks at successive pairs of numbers
;;   if the first number in the pair is smaller than the second, multiply them
;;   otherwise, add them
;;
;; Repeat until only one number remains
;;
;; When given an empty list, return 1 (i.e. no quantity was specified)
(define (coalesce-quantity xs)
  (define (helper xs)
    (cond
      ((< (length xs) 2) xs)
      (else (let ((fst (car xs))
                  (snd (cadr xs))
                  (rest (cddr xs)))
              (helper
                (cons (if (> fst snd)
                        (+ fst snd)
                        (* fst snd)) (helper rest)))))))
  (cond ((null? xs) 1)
        (else (car (helper (reverse xs))))))


;; Map the program's command line arguments into a list of gimme specs
(define (parse-command-line args)
  ; strip punctuation from words, except for '-'
  (define cs:punct-minus- (char-set-delete char-set:punctuation #\-))

  (let loop ((args (map (lambda (s) (string-delete cs:punct-minus- s)) args))
             (quant '()))
    (if (null? args)
      '()

      (let* ((this-arg (car args))
             (this-num (string->number this-arg)))

        (if (and (number? this-num) (> this-num 0))
          ;; numeric quantifiers
          (loop (cdr args) (cons this-num quant))

          ;; textual quantifiers and items
          (case (string->symbol (string-downcase this-arg))

            ;; types of items this program can return
            ((number numbers)
             (cons
               (list 'number (coalesce-quantity quant)
                     (if (and
                           (>= (length args) 5)
                           (or
                             (string-ci=? "from" (cadr args))
                             (string-ci=? "between" (cadr args)))
                           (or
                             (string-ci=? "to" (cadddr args))
                             (string-ci=? "and" (cadddr args))))
                       (list (caddr args) (car (cddddr args)))
                       '()))
               (loop (cdr args) '())))

            ((version versions)
             (cons (cons 'version (coalesce-quantity quant)) (loop (cdr args) '())))

            ((coin-flip coin-flips coinflip coinflips)
             (cons (cons 'coin-flip (coalesce-quantity quant)) (loop (cdr args) '())))

            ((hex-color hex-colors hexcolor hexcolors hex hexes)
             (cons (cons 'hex-color (coalesce-quantity quant)) (loop (cdr args) '())))

            ((time times local-time local-times localtime localtimes)
             (cons (cons 'local-time (coalesce-quantity quant)) (loop (cdr args) '())))

            ((help helps)
             (cons (cons 'help (coalesce-quantity quant)) (loop (cdr args) '())))

            ;; dictionaries
            ((adjective adjectives)
             (cons (cons 'adjective (coalesce-quantity quant)) (loop (cdr args) '())))

            ((adverb adverbs)
             (cons (cons 'adverb (coalesce-quantity quant)) (loop (cdr args) '())))

            ((color colors)
             (cons (cons 'color (coalesce-quantity quant)) (loop (cdr args) '())))

            ((csscolor csscolors)
             (cons (cons 'csscolor (coalesce-quantity quant)) (loop (cdr args) '())))

            ((fancycolor fancycolors)
             (cons (cons 'fancycolor (coalesce-quantity quant)) (loop (cdr args) '())))

            ((interjection interjections)
             (cons (cons 'interjection (coalesce-quantity quant)) (loop (cdr args) '())))

            ((name names)
             (cons (cons 'name (coalesce-quantity quant)) (loop (cdr args) '())))

            ((noun nouns)
             (cons (cons 'noun (coalesce-quantity quant)) (loop (cdr args) '())))

            ((surname surnames)
             (cons (cons 'surname (coalesce-quantity quant)) (loop (cdr args) '())))

            ((verb verbs)
             (cons (cons 'verb (coalesce-quantity quant)) (loop (cdr args) '())))

            ((word words)
             (cons (cons 'word (coalesce-quantity quant)) (loop (cdr args) '())))

            ;; quantities
            ((one a an)
             (loop (cdr args) (cons 1 quant)))

            ((two brace braces pair pairs couple couples)
             (loop (cdr args) (cons 2 quant)))

            ((three trio trios)
             (loop (cdr args) (cons 3 quant)))

            ((few)
             (loop (cdr args) (cons (a-number-between 3 5) quant)))

            ((four quartet quartets)
             (loop (cdr args) (cons 4 quant)))

            ((some several)
             (loop (cdr args) (cons (a-number-between 4 7) quant)))

            ((five handful handfuls quintent quintents)
             (loop (cdr args) (cons 5 quant)))

            ((pack packs)
             (loop (cdr args) (cons (a-number-between 5 21) quant)))

            ((six six-pack)
             (loop (cdr args) (cons 6 quant)))

            ((seven)
             (loop (cdr args) (cons 7 quant)))

            ((eight)
             (loop (cdr args) (cons 8 quant)))

            ((nine)
             (loop (cdr args) (cons 9 quant)))

            ((ten)
             (loop (cdr args) (cons 10 quant)))

            ((eleven)
             (loop (cdr args) (cons 11 quant)))

            ((dozen twelve)
             (loop (cdr args) (cons 12 quant)))

            ((thirteen)
             (loop (cdr args) (cons 13 quant)))

            ((fourteen)
             (loop (cdr args) (cons 14 quant)))

            ((fifteen)
             (loop (cdr args) (cons 15 quant)))

            ((sixteen)
             (loop (cdr args) (cons 16 quant)))

            ((seventeen)
             (loop (cdr args) (cons 17 quant)))

            ((eighteen)
             (loop (cdr args) (cons 18 quant)))

            ((nineteen)
             (loop (cdr args) (cons 19 quant)))

            ((twenty twenties)
             (loop (cdr args) (cons 20 quant)))

            ((thirty thirties)
             (loop (cdr args) (cons 30 quant)))

            ((forty forties)
             (loop (cdr args) (cons 40 quant)))

            ((fifty fifties)
             (loop (cdr args) (cons 50 quant)))

            ((sixty sixties)
             (loop (cdr args) (cons 60 quant)))

            ((seventy seventies)
             (loop (cdr args) (cons 70 quant)))

            ((eighty eighties)
             (loop (cdr args) (cons 80 quant)))

            ((ninety ninetys)
             (loop (cdr args) (cons 90 quant)))

            ((hundred hundreds)
             (loop (cdr args) (cons 100 quant)))

            ((bunch many lots)
             (loop (cdr args) (cons (a-number-between 8 15) quant)))

            ((mess oodles multitude ton crapton grundle swarm crowd flock)
             (loop (cdr args) (cons (a-number-between 20 55) quant)))

            ((thousand)
             (loop (cdr args) (cons 1000 quant)))

            ((gross)
             (loop (cdr args) (cons 144 quant)))

            ((bajillion gazillion)
             (loop (cdr args) (cons (a-number-between 1000 10000) quant)))

            ((myriad tenthousand ten-thousand)
             (loop (cdr args) (cons 10000 quant)))

            (else
              (loop (cdr args) quant))))))))


(define (help #!optional spec)
  (define message #<#USAGE
Gimme.scm v#*VERSION*

Usage: gimme [[quantifier ...] item] ...

    Where "item" is one of: ad{jective,verb} | {fancy,css,hex,}color
                            | interjection | word | {sur,}name | noun | verb
                            | number | version | coin-flip | time

    and "quantifier" is: {1..MAX_INT} | {one..nineteen} | {twenty..hundred}
                            | {a,an} | pair | brace | couple | few | handful
                            | some | several | bunch | many | lots | dozen
                            | ton | crapton | grundle | hundred | thousand
                            | myriad | bajillion | gazillion

    Quantifiers may be combined in more-or-less the natural way:
      "two dozen"
      "fifty five",
      "a hundred and twenty seven",
      or "twelve thousand three hundred and forty five"

    Unrecognized words are ignored, as is punctuation (except for "-").

Examples:
  $ gimme a number
  -92

  $ gimme some names
  Gabrielle
  Ruth
  Oliver
  Joan
  Ashley

  $ gimme a noun, twenty two verbs, and a handful of numbers between 10 and -10
  puritan

  carry
  ... [20 lines elided]
  flash

  -9
  -10
  8
  -4
  5

USAGE
)
  (if spec
    (unfold zero? (lambda x message) sub1 (cdr spec))
    (print message)))


(define (gimme args)
  (if (zero? (length args))
    (help)

    (for-each
      (lambda (s)
        (case (car s)
          ((version)
           (let loop ((n (cdr s)))
             (when (not (zero? n))
               (print "Gimme.scm v" *VERSION*)
               (loop (sub1 n)))))

          ((adjective adverb color csscolor fancycolor
              interjection name noun surname verb word)
           (for-each print (random-words s)))

          ((number coin-flip hex-color local-time help)
           (for-each print ((eval (car s)) s))))
        (newline))

      (begin
        (seed-prng-from-urandom!)
        (parse-command-line args)))))

(gimme (command-line-arguments))
