CSC ?= csc
CSC_OPTIONS ?= -static
INSTALL ?= install
RM      ?= rm
RMDIR   ?= rmdir
SUDO    ?= sudo

DICTDIR ?= /usr/local/share/dict
BINDIR  ?= /usr/local/bin
DICTIONARIES = adjective adverb color csscolor fancycolor interjection \
			   name noun surname verb word


test: gimme
	./gimme a bunch of versions, a thirteen names, twenty seven colors, some interjections and a handful of handfuls of numbers

gimme: gimme.scm
	$(CSC) $(CSC_OPTIONS) $^

install: gimme
	$(SUDO) $(INSTALL) -d -m 775 $(BINDIR)
	$(SUDO) $(INSTALL) -p -m 755 -t $(BINDIR) $^
	$(SUDO) $(INSTALL) -d -m 775 $(DICTDIR)
	$(SUDO) $(INSTALL) -p -m 644 -t $(DICTDIR) $(addprefix dict/,$(DICTIONARIES))

uninstall:
	-$(RM) $(BINDIR)/gimme
	-$(RM) -f $(addprefix $(DICTDIR)/,$(DICTIONARIES))
	-$(RMDIR) $(DICTDIR)

clean:
	-rm -f *.o *.c *.link gimme core

.PHONY: clean install test
